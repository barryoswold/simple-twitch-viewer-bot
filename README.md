## Simple Twitch Viewer Bot

Being in this industry for a while, we wanted to give something back. This repo will serve as supplying a simple "base" for anyone interested in getting started with a viewer bot. This is a free version of what we offer at [Viewerlabs](https://www.viewerlabs.com) - adjust as you see fit.
